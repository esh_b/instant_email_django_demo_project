from django.db import models
from django.dispatch import receiver
from app_prim.models import user_profiles
from django.db.models.signals import post_save

@receiver(post_save, sender=user_profiles)
def profile_changed(sender, **kwargs):
	user = kwargs['instance']

	if(kwargs['created']):
		#Insert
		pass
	else:
		#Update
		updated_fields = kwargs['update_fields']
		pass

