from django.apps import AppConfig


class AppSecConfig(AppConfig):
    name = 'app_sec'
