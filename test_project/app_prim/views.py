from django.shortcuts import render
from app_prim.models import user_profiles

# Create your views here.
def index(request):
	#Create user
	"""
	user = user_profiles(name='name_1', country='country_1')
	user.save()
	"""
	
	#User update - No signal triggered
	#user_profiles.objects.filter(id=5).update(country='JP')

	#Update using queryset
	"""
	users = user_profiles.objects.filter(id=20)
	for user in users:
		user.country = 'latest'
		user.save(update_fields=['country'])
	"""

	#Update a single row in db
	user = user_profiles.objects.get(id=21)
	user.name = 'hanamura yuki'
	user.country = 'JP'
	user.save(update_fields=['name', 'country'])

	return