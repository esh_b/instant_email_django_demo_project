test_project contains two apps:
* app_prim
* app_sec

**app_prim:**
* The app which updates or insert rows in the database table.

**app_sec:**
* The app which receives the updates or inserts to any values in the specified database table.

